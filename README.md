# 概要

kotlinを業務で使うためのフレームワークテンプレートを実装しようと思います

## 方針

- 早い(開発もパフォーマンスも)
- 環境依存でない
- 明示的な(ソースコードが追っかけやすい)

# 0. 環境構築

## 前提条件

- docker
- docker-compose

## 1. 自分の変数を設定

dockerの起動設定をテンプレからコピーして自分なりに修正

特に何もしなくても動くはず。ポートが他のコンテナと被ってないか注意

```
cp docker-compose.yml.sample docker-compose.yml
```

環境変数をテンプレからコピーして自分なりに修正

特に何もしなくても動くはず。

```
cp docker-compose.yml.sample docker-compose.yml
```

## 2. docker起動

```
docker-compose up -d --build
```

`docker-compose ps`で起動状況を確認。jooby-webとjooby-mysql57が存在することを確認

ここで失敗していた場合は、`dockerコマンド`または`Dockerfile`または`docker-compose.yml`のどれかに不備があるはず

## 3. dockerないに侵入してビルド

侵入

```
docker exec -it jooby-web /bin/bash
```

まずはビルドしてみる

```
./gradlew build
```

次にサーバーを実際に動かしてみる

```
./gradlew joobyRun
```

`localhost:8080`でアクセスすると、`Hello World`になっているかチェック

## 4. DB確認

mysqlに接続できるクライアントツールで、mysqlのコンテナに対してアクセスする。

ポートやパスワードなどの設定は `docker-compose.yml`に書いてあるのでそれを確認。

joobyがアクセスするデータベース内に、 `hibernate_sequence`テーブルが存在していたらDBとjoobyの接続がうまくいっている

