package starter.kotlin.entity

import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id


@Entity(name = "contacts")
class Contact {

    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private var id: Int? = null

    private val name: String? = null

    var notes: String? = null

    protected var website: String? = null

    private var starred: Int = 0

    private var password: String? = null


    fun setPassword(rawPassword: String) {
        this.password = rawPassword
    }

    fun review(isUp: Boolean) {
        if (isUp) {
            this.starred++
        } else {
            this.starred--
        }
    }
}
