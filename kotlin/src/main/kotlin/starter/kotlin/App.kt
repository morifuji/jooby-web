package starter.kotlin

import org.jooby.Jooby.*
import org.jooby.Kooby
import org.jooby.json.Jackson
import org.jooby.apitool.ApiTool
import org.jooby.mvc.*

import org.slf4j.LoggerFactory
import javax.inject.Inject

import org.jooby.jdbc.*
import org.jooby.hbm.*
import starter.kotlin.entity.Contact

data class Hello(val hello: String)

@Path("/api/pets")
class Pets @Inject constructor() {

    val logger = LoggerFactory.getLogger(Pets::class.java)

    @GET
    fun list(): List<String> {
        logger.info("ほげほげええ:{},    {}", "うんち", "うごうご")
        return listOf("1", "", "3", "$")
    }
}

data class People(val name: String, var age: Int)


/**
 * Gradle Kotlin stater project.
 */
class App : Kooby({
    use(Jackson())
    use(ApiTool().swagger().raml())
    use(Jdbc())
    use(Hbm().classes(Contact::class.java))
    use(Pets::class)

    get("/api/beer/") { req ->
        require(UnitOfWork::class.java).apply { em ->
            val c = Contact()
            c.notes = "ノートだよ！！！そのままinsert!!"
            c.review(true)
            c.review(true)
            c.setPassword("ほげほげ")

            em.save(c)

            c.review(false)
            c.notes = "修正済み(´・ω・`)"
            em.save(c)

            em.createQuery("from contacts").resultList
        }
    }

    get("/hello/{name}") { Hello(param("name").value()) }

//    get("/:name") { req ->
//        val name = req.param("name").value()
//        "Hi $name!"
//    }

    get {
        val name = param("name").value("Kotlin")
        "Hello $name!"
    }

    get("/array") { req ->
        val arr = listOf(1,2,3,4,5)
        arr
    }

    get("/map") { req ->
        val map = mapOf("hoge" to "fuga", "りんご" to "ごりら")
        map
    }

    get("/data_class/:name") {req->
        val people = People("金田哲夫", 19)
        people
    }

    post {
        val aaa = "あああああ"
        val qqq = "うううう"
        "ggggg"
    }

    get("/hogehoge") { "hey jooby" }

    post("/huga") { req -> req.body() }
})

/**
 * Run application:
 */
fun main(args: Array<String>) {
    run(::App, args)
}
