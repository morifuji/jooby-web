## display command list of gradle

```
./gradlew tasks
```

## build to jar file

```
./gradlew build
```

## run development server

```
./gradlew joobyRun
```

## create docker-image for deploy

```
./gradlew --stop

./gradlew docker
```

Output file is in `{gitRootDir}/kotlin/build/libs/kotlin-gradle-starter-1.0.jar`

## test

```
./gradlew test
```

## help

* Read the [module documentation](http://jooby.org/doc/lang-kotlin)
* Join the [channel](https://gitter.im/jooby-project/jooby)
* Join the [group](https://groups.google.com/forum/#!forum/jooby-project)
